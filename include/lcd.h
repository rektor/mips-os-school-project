/*! \file lcd.h
    \brief Functions for using the Malta LED display
    
    Functions for using the Malta LED display
    From UU compsys example files
    http://www.it.uu.se/edu/course/homepage/datsystDV/ht07/Project/files
*/
#ifndef __LCD_H__
#define __LCD_H__

#include "malta.h"

void init_lcd();
void display_word(uint32_t word);

#endif
