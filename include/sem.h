/*! \file sem.h
    \brief Defines the semaphores
    
    Defines the semaphores and declares it's functions
*/

#ifndef __SEM_H__
#define __SEM_H__

#include <types.h>

/*! \var sem_t
    \brief Semaphore type
*/
typedef uint32_t sem_t;

void sem_init(sem_t *, uint32_t);
void sem_signal(sem_t *);
void sem_wait(sem_t *);

#endif
