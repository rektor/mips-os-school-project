/*! \file kmalloc.h
    \brief Types and declarations for kmalloc, kfree and morecore
    
    Types and declarations for kmalloc, kfree and morecore.
	Code from K&R section 8.7: Example - A storage allocator
	morecore modified to use array allocated in BSS instead of sbrk()

*/
#ifndef __KMALLOC_H__
#define __KMALLOC_H__

#include "types.h"

typedef long Align; 			/* for alignment to long boundary */

union header { 					/* block header */
	struct {
		union header *ptr; 		/* next block if on free list */
		uint32_t size; 			/* size of this block */
	} s;
	Align x; 				  	/* force alignment of blocks */
};

typedef union header Header;
void kmalloc(uint32_t nbytes);
void* exc_malloc(uint32_t nbytes);
void kfree(void *ap);
Header *morecore(uint32_t nu);

#endif
