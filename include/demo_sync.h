/*! \file demo_sync.h
    \brief A demo program showing off semaphores
    
    A simple demo program meant to demonstate synchronization
    of processes with the help of semaphores
*/

#ifndef __demo_sync_h__
#define __demo_sync_h__

void sync_main();

#endif
