/*! \file shell.h
    \brief Header for shell
    
    header for shell
*/

#ifndef __SHELL_H__
#define __SHELL_H__
#include "syscall.h"
#include "timer.h"
#include "uart.h"
#include "scheduler.h"


void shell_main();
void answer();


#endif
