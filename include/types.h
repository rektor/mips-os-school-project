/*! \file regdef.h
    \brief Defines types
    
    Defines types
    
    From UU compsys example files
    http://www.it.uu.se/edu/course/homepage/datsystDV/ht07/Project/files
*/

#ifndef TYPES_H
#define TYPES_H

/*! \var uint8_t
    \brief unsigned char
*/
typedef unsigned char  uint8_t;
/*! \var uint16_t
    \brief unsigned short
*/
typedef unsigned short uint16_t;
/*! \var uint32_t
    \brief unsigned int
*/
typedef unsigned int   uint32_t;

/*! \var int8_t
    \brief signed char
*/
typedef signed char  int8_t;
/*! \var int16_t
    \brief signed short
*/
typedef signed short int16_t;
/*! \var int32_t
    \brief signed int
*/
typedef signed int   int32_t;

#endif
