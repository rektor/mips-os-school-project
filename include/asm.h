/*! \file asm.h
    \brief Declarations for functions in boot.S
    
    Holds declarations for functions in boot.S
    From UU compsys example files
    http://www.it.uu.se/edu/course/homepage/datsystDV/ht07/Project/files
*/
#ifndef ASM_H
#define ASM_H

#include "types.h"
#include "pcb.h"


/* Functions provided by 'boot.S' */
uint32_t kset_sr(uint32_t and_mask, uint32_t or_mask);
uint32_t kget_sr();
uint32_t kset_cause(uint32_t and_mask, uint32_t or_mask);
uint32_t kget_cause();
void kload_timer(uint32_t timer_value);
void kset_registers(registers_t* regs);
registers_t* kget_registers();
void kdebug_magic_break();
uint32_t ksyscall(uint32_t type, uint32_t arg1, uint32_t arg2, uint32_t arg3);
uint32_t kget_count();

/*
 * kinit is called at startup, contains application-specific
 * system initialisation.
 * Applications should make sure that 'kset_registers' is
 * called, to that the exception handler can save registers.
 */
void kinit();

/*
 * kexception is called when an exception occurs, after registers
 * have been saved. Use 'kget_registers' to access the saved
 * registers, and 'kset_registers' to update the entire register
 * set when 'kexception' returns (useful for task switching).
 */
void kexception();

#endif
