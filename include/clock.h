
#ifndef ___CLOCK___
#define ___CLOCK___

#include "types.h"

void clock_init();
uint32_t curr_time();

#endif
