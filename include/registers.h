/*! \file registers.h
    \brief Struct for storing registers in RAM
    
    Defines struct for storing registers in RAM
    
    From UU compsys example files
    http://www.it.uu.se/edu/course/homepage/datsystDV/ht07/Project/files
*/

#ifndef REGISTERS_H
#define REGISTERS_H

#include "types.h"

/*! \struct registers_t
    \brief Struct for storing registers in RAM. Must correspond with offsets in asm_regs.h.
*/
typedef struct
{
    uint32_t at_reg;    /* offset   0 */ /**< at register */
    uint32_t v_reg[2];  /* offset   4 */ /**< v0-1 registers */
    uint32_t a_reg[4];  /* offset  12 */ /**< a0-3 registers */
    uint32_t t_reg[10]; /* offset  28 */ /**< t0-9 registers */
    uint32_t s_reg[8];  /* offset  68 */ /**< s0-7 registers */
    uint32_t sp_reg;    /* offset 100 */ /**< sp register */
    uint32_t fp_reg;    /* offset 104 */ /**< fp register */
    uint32_t ra_reg;    /* offset 108 */ /**< ra register */
    uint32_t epc_reg;   /* offset 112 */ /**< epc register */
    uint32_t gp_reg;    /* offset 116 */ /**< gp register */
} registers_t;          /* size   120 */

#endif
