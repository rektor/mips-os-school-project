/*! \file syscall.h
    \brief Defines syscalls and their functions
    
    Defines syscalls and their functions
*/

#ifndef __SYSCALL_H__
#define __SYSCALL_H__

#include "types.h"
#include "pcb.h"
#include "ipc.h"

#define SC_PRINTLINE 0	
#define SC_READLINE 1
#define SC_SLEEP 2
#define SC_MALLOC 3
#define SC_FREE 4
#define SC_FORK 5
#define SC_GETID 6
#define SC_GETPID 7
#define SC_EXIT 8
#define SC_PRINTPROCS 9
#define SC_WAIT 10
#define SC_WAITID 11
#define SC_EXEC 12
#define SC_SENDMSG 13
#define SC_RECEIVE 14
#define SC_SEM_SLEEP 15
#define SC_SEM_WAKEUP 16
#define SC_EXEC1 17
#define SC_EXEC2 18
#define SC_EXEC3 19
#define SC_SET_TTY 20
#define SC_GET_TTY 21
#define SC_RECEIVE_WAIT 22

#define DEV_TTY0 0
#define DEV_TTY1 1


void handle_syscall(uint32_t type,
						uint32_t arg1,
						uint32_t arg2,
						uint32_t arg3);

void printline(char* line);
void sleep(uint32_t msecs);
uint32_t readline(char* buf);

void printf(char* line, ...); 
void debug_printf(char* line, ...);
void *malloc(uint32_t bytes);
void free(void *ptr);
int32_t fork();
uint32_t getid();
uint32_t getpid();
void exit(uint32_t ret);
uint32_t wait(uint32_t *status);
uint32_t waitid(uint32_t *status, uint32_t id);
void exec(void* func, char *name);
void printprocs();
uint32_t receive(mail_t msg_space);
void receive_wait(mail_t msg_space);
int32_t send(uint32_t data, uint32_t ctrl, uint32_t pid);
void exec1(void* func, char *name, uint32_t arg1);
uint32_t set_tty(uint32_t tty);
uint32_t get_tty();

#endif
