/*! \file uart.h
    \brief Defines uart functions and bounded fifo used by them
    
    Defines uart functions and bounded fifo used by them
    
    Based on UU compsys example files
    http://www.it.uu.se/edu/course/homepage/datsystDV/ht07/Project/files
*/
#ifndef __UART_H__
#define __UART_H__

#define FIFO_SIZE 2048

void init_uart();
void printstr(char print_array[]);
void handle_uart();
int set_input(char *buf, uint32_t ntty);

/* A simple FIFO queue of bounded size. */
/*! \struct bounded_fifo_t
    \brief A simple FIFO queue of bounded size
*/
typedef struct {
	uint8_t  buf[FIFO_SIZE];			/**< space for queue */
	uint32_t length;					/**< length of queue */
	uint32_t get;						/**< position to get from */
	uint32_t put;						/**< position to put in */
} bounded_fifo_t;


#endif
