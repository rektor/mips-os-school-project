/*! \file scheduler.h
    \brief Process status definitions and scheduler functions
    
    Process status definitions and scheduler functions.
*/

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__
#include "pcb.h"
#include "sem.h"
#include "asm.h"
#include "malta.h"
#include "uart.h"
#include "lcd.h"
#include "syscall.h"
#include "timer.h"
#include "shell.h"
#include "pcb.h"
#include "ipc.h"

/* Timer 67MHz (?)  compsys HT2007 example*/
static const int timer_usec = 67;
static const int timer_msec = 67000;

/* Process statuses */
#define STAT_WAITING 0
#define STAT_READY 1
#define STAT_RUNNING 2
#define STAT_WAIT_IO 3
#define STAT_WAIT_TIME 4
#define STAT_WAIT_SEM 5
#define STAT_WAIT_PARENT 6
#define STAT_WAIT_MAIL 7
#define STAT_DEAD 9999

void handle_scheduler();
void init_scheduler();
void sched_line_read(uint32_t n_chars, uint32_t tty);
void sched_wait_input(char *buf);
uint32_t handle_timer(uint32_t time);
void sched_sleep(uint32_t msecs);
void sched_printprocs();
void sched_fork();
void sched_wait(uint32_t *status);
void sched_waitid(uint32_t *status, uint32_t pid);
void sched_exit(uint32_t stat);
void sched_exec(void *func, char *name);
void sched_exec1(void *func, char *name, uint32_t arg1);
void sched_getid();
void sched_getpid();
uint32_t create_process(void *func, char *name, uint32_t arg1);
pcb_t *find_pcb(uint32_t pid);

void sem_sleep(sem_t *);
void sem_wakeup(sem_t *);

void set_return(uint32_t ret);
void sched_set_tty(uint32_t tty);
void sched_get_tty();
uint32_t k_get_tty();

void ipc_receive(mail_t msg_space);
void ipc_receive_wait(mail_t msg_space);
void ipc_send(uint32_t data, uint32_t ctrl, uint32_t pid);
#endif
