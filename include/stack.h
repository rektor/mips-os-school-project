/*! \file stack.h
    \brief Defines the stack
    
    Defines the stack
*/

#ifndef __STACK_H__
#define __STACK_H__

#include "types.h"

#define STACK_SIZE 1024

/*! \struct stack_t
    \brief Struct for the stack space
*/
typedef struct {
	uint32_t stackspace[STACK_SIZE - 1]; /**< Stack space */
	uint32_t endstack;					 /**< End of stack space */
} stack_t;

#endif
