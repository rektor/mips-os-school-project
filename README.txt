==== COURSE ==== 

Process Oriented Programming (1DT049) Spring 2012

Department of Information Technology 
Uppsala university


==== GROUP ==== 

17 


==== PROJECT NAME ==== 

17th wizard of OS


==== PROJECT DESCRIPTION ==== 

A simple MIPS operating system for a simulated MIPS 4Kc on a malta board. 


==== GROUP MEMBERS ==== 

YYMMDD-XXXX Firstname.Lastname.1234@student.uu.se
YYMMDD-XXXX Firstname.Lastname.1234@student.uu.se
YYMMDD-XXXX Firstname.Lastname.1234@student.uu.se
YYMMDD-XXXX Firstname.Lastname.1234@student.uu.se


==== MAY THE SOURCE BE WITH YOU ==== 

To compile the kernel you need a toolchain that builds for mips.
We have used CodeSourcery CodeBench Lite

To run the simulation you need Virtutech simics.

==== TO COMPILE ==== 

To build the kernel run:
   make bin/kernel

To build the kernel and run it in simics run:
   make do_boot_kernel

==== GENERATE DOCUMENTATION ====

To generate documentation run:
   doxygen
