
#include "shell.h"
#include "syscall.h"
#include "demo_sync.h"
#include "demo_msg.h"

//char * buf;


uint32_t strcmp(char *a, char *b) {

  while(*a != '\0' && *b != '\0') {
    if(*a < *b)
      return -1;
    if(*a > *b)
      return 1;

    a++;
    b++;

  }
  if(*a == '\0') {
    if(*b == '\0') {
      return 0;
    } else {
      return -1;
    }
  } else {
    //b is at '\0' but not a
    return 1;
  }
  return 0;
}


void shell_processcommand(char * buf, int size)
{
	char arg[20];
	char cmd[20];
	uint32_t exit_status;
  
	int i = 0;
	int j = 0;
	while(buf[i] != '\0' && buf[i] != ' ') { 
		cmd[i] = buf[i];
		i++;

	}
	cmd[i] = '\0';
	while(buf[i] != '\0' && i < size) { 
		if(buf[i] != ' ') {
			while(buf[i] != ' ' && buf[i] != '\0') { 
				arg[j] = buf[i]; 
				j++;
				i++;

			}
		}
		else
			i++;
	} 

  
	if(strcmp(cmd,"timer") == 0) {
		int i = fork();
		switch(i) {
		case -1:
			printf("Error, exiting... \n"); 
			exit(0);
			break;
		case 0:
			exec(&timer_start,"timer");  
			break;
		default:
			break;
		}
    

  }
  else if(strcmp(cmd,"what?") == 0) { 
    int i = fork();
    switch(i) {
    case -1:
      printf("Error\n");
      free(cmd);
      free(arg);
      exit(666);
    case 0:
      exec(&answer,"lol");
      break;
    default:
      waitid(&exit_status,i);
      break;
    }
  }
  else if(strcmp(cmd,"ifl") == 0) { 
    int i = fork();
    switch(i) {
    case -1:
      printf("Error\n");
      free(cmd);
      free(arg);
      exit(666);
    case 0:
      exec(&russianroulette,"ruski");
      break;
    default:
      waitid(&exit_status,i);
      break;
    }
  }
  else if(strcmp(cmd,"ps") == 0) {
    printprocs();

    
	} else if(strcmp(cmd, "demo_sync") == 0) {
		int a = 0;
		a = fork();
		if(a == 0) {
			exec(&sync_main, "demo_sync");
		} else {
			uint32_t retstat;
			wait(&retstat);
		}
	} else if(strcmp(cmd, "demo_msg") == 0) {
		int a = 0;
		a = fork();
		if(a == 0) {
			exec(&demo_msg_main, "demo_msg");
		} else {
			uint32_t retstat;
			wait(&retstat);
		}
	}
	else
		printf("Felaktigt kommando \n");

}


void shell_readLine(char *buf)
{
   if(!buf)
    printf("jävla error \n");
  
  int n = readline(buf);

  if(n > 80)
    printf("jävla mycket tecken \n");

  shell_processcommand(buf,n);
  

}



void shell_main()
{
	char *buf = (char*)malloc(sizeof(char) * 80);
  while(1) {

    printf("$");
  
    shell_readLine(buf);
  
  }

}



