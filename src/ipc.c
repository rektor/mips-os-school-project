//functions and values used by interprocess communication.

#include "ipc.h"

//Allocates, sets and returns a new message element. returns 0 on failure.
mail_t mk_mail(uint32_t data, uint32_t ctrl) {
	mail_t m = (mail_t)exc_malloc(sizeof(msg_t));
	if (!m) return m;
	m->next = 0;
	m->data = data;
	m->ctrl = ctrl;
	return m;
}

//Takes message element mail and puts behind last element in mailbox, if others are N/A mail is new mailbox
void push_mail(mail_t m, pcb_t *p) {
	if(p->mail) {
		while(p->mail->next) p->mail = p->mail->next;
		p->mail->next = m;
		}
	else p->mail = m;
}

//Removes a message element, returns data-field.
void rm_mail(mail_t msg) {
	msg->next = 0;
	msg->data = 0;
	msg->ctrl = 0;
	free(msg);
}

// send, receive, receive_wait moved to scheduler.c for access to waiting lists.
// named there as ipc_send, ipc_receive, ipc_receive_wait




