/*! \file syscall.c
    \brief System calls

    Entry points for system calls for user and entry point to system calls for kexception
*/
#include "syscall.h"
#include "asm.h"
#include "uart.h"
#include "kmalloc.h"
#include "scheduler.h"
#include "pcb.h"

/*! handle_syscall - Handles all system calls
    \param type  The type of system call, defines in syscall.h
    \param arg1  First argument to syscall
    \param arg2  Second argument to syscall
    \param arg3 Third argument to syscall
	\return Depends on the type of system call
*/
void handle_syscall(uint32_t type, uint32_t arg1, uint32_t arg2, uint32_t arg3){
	switch(type) {
	case SC_PRINTLINE:
		printstr((char*) arg1);
		break;
	case SC_READLINE:
		sched_wait_input((char*) arg1);
		break;
	case SC_SLEEP:
		sched_sleep(arg1);
		break;
	case SC_MALLOC:
		kmalloc(arg1);
		break;
	case SC_FREE:
		kfree((void*)arg1);
		break;
	case SC_FORK:
		sched_fork();
		break;
	case SC_GETID:
		sched_getid();
		break;
	case SC_GETPID:
		sched_getpid();
		break;
	case SC_EXIT:
		sched_exit(arg1);
		break;
	case SC_PRINTPROCS:
		sched_printprocs();
		break;
	case SC_WAIT:
		sched_wait((uint32_t*)arg1);
		break;
	case SC_WAITID:
		sched_waitid((uint32_t*)arg1, arg2);
		break;
	case SC_EXEC:
		sched_exec((void*)arg1, (char*)arg2);
		break;
	case SC_SENDMSG:
		ipc_send(arg1, arg2, arg3);
		break;
	case SC_RECEIVE:
		ipc_receive((mail_t)arg1);
		break;
	case SC_RECEIVE_WAIT:
		ipc_receive_wait((mail_t)arg1);
		break;
	case SC_SEM_SLEEP:
		sem_sleep((sem_t *)arg1);
		break;
	case SC_SEM_WAKEUP:
		sem_wakeup((sem_t *)arg1);
		break;
	case SC_EXEC1:
		sched_exec1((void*)arg1, (char*)arg2, arg3);
		break;
	case SC_SET_TTY:
		sched_set_tty(arg1);
		break;
	case SC_GET_TTY:
		sched_get_tty();
		break;
	}
}

/*! Makes a print line system call
    \param line A null terminated string
    \return void.
*/
void printline(char* line) {
	ksyscall((uint32_t) SC_PRINTLINE, (uint32_t) line, 0, 0);
}

//! Makes a readline system call.
/*! Reads a line from tty0 and stores it as a null terminated string in the memory pointed to
    by buf.
    \param buf Pointer to the memory where readline will store the read line.
    \return Number of characters read, including '\0'.
*/
uint32_t readline(char* buf) {
	return ksyscall((uint32_t) SC_READLINE, (uint32_t) buf, 0, 0);
}

//! Makes a sleep system call
/*! Puts the process to sleep for msecs milliseconds
    \param msecs The number of milliseconds the process will be asleep.
 */
void sleep(uint32_t msecs) {
	ksyscall((uint32_t) SC_SLEEP, msecs, 0, 0);
}
	

//! Makes a malloc system call
/*! Allocates memory of the size specified by bytes.
    \param bytes Size of memory block to allocate in bytes.
    \return Pointer to the start of the allocated memory block.
 */
void *malloc(uint32_t bytes) {
	return (void*) ksyscall((uint32_t) SC_MALLOC, bytes, 0, 0);
}

//! Makes a free system call
/*! Frees the memory pointed to by ptr
    \param ptr Pointer to beginning of memory block allocated with malloc.
 */
void free(void *ptr) {
	ksyscall((uint32_t) SC_FREE, (uint32_t)ptr, 0, 0);
}

//! Makes a fork system call.
/*! Makes a copy of the current process with it as it's parent.
    \return Id of child in parent. 0 in child.
*/
int32_t fork() {
	return (int32_t)ksyscall((uint32_t) SC_FORK, 0, 0, 0);
}


//! Make an printprocs system call
/*! Make an printprocs system call. All active processes will be printed to tty of current process.
*/
void printprocs() {
	ksyscall((uint32_t) SC_PRINTPROCS, 0, 0, 0);
}
//! Make a wait system call
/*! Make a wait system call. It's a blocking system call unless the
    child is already waiting.
    \param status Pointer to uint32_t where child's exit status will be saved.
    \return Id of child.
*/

uint32_t wait(uint32_t *status) {
	return ksyscall((uint32_t) SC_WAIT, (uint32_t)status, 0, 0);
}

/*! \brief Make a wait system call
    
    Make a wait system call. It's a blocking system call unless the
    child is already waiting.
    
    \param status Pointer to uint32_t where child's exit status will be saved.
    \param id Id of child to wait for.
    \return Id of child.
*/
uint32_t waitid(uint32_t *status, uint32_t id) {
	return ksyscall((uint32_t) SC_WAITID, (uint32_t)status, id, 0);
}

//! Make an exit system call
/*! Make an exit system call. It kills the process and returns exit status to parent.
    \param ret Return status. Will be returned to the parent's wait system call.
*/
void exit(uint32_t ret) {
	ksyscall((uint32_t)SC_EXIT, ret, 0, 0);
}

//! Make an exec system call
/*! Make an exec system call. The process func will be loaded into the pcb and the
    process will be given the name in name.
    \param func Pointer to function to be loaded into the process.
    \param name The name to be given to the process.
*/
void exec(void *func, char *name) {
	ksyscall((uint32_t)SC_EXEC, (uint32_t)func, (uint32_t)name, 0);
}

//! Make an exec1 system call
/*! Make an exec1 system call. The process func will be loaded into the pcb and the
    process will be given the name in name.
    \param func Pointer to function to be loaded into the process.
    \param name The name to be given to the process.
    \param arg1 Argument to func.
*/
void exec1(void *func, char *name, uint32_t arg1) {
	ksyscall((uint32_t)SC_EXEC1, (uint32_t)func, (uint32_t)name, arg1);
}

//! Make a getid system call
/*! Make an getid system call. Returns the id of the current process.
    \return The id of the current process.
*/
uint32_t getid() {
	return ksyscall((uint32_t)SC_GETID, 0, 0, 0);
}

//! Make a getpid system call
/*! Make an getpid system call. Returns the id of the parent of the current process.
    \return The id of the parent of the current process.
*/
uint32_t getpid() {
	return ksyscall((uint32_t)SC_GETPID, 0, 0, 0);
}

int32_t send(uint32_t data, uint32_t ctrl, uint32_t pid) {
	return (int32_t)ksyscall((uint32_t) SC_SENDMSG, data, ctrl, pid);
}

uint32_t receive(mail_t msg_space) {
	return ksyscall((uint32_t) SC_RECEIVE, (uint32_t)msg_space, 0, 0);
}

void receive_wait(mail_t msg_space)  {
	ksyscall((uint32_t) SC_RECEIVE_WAIT, (uint32_t)msg_space, 0, 0);
}

//! Make a set_tty system call
/*! Make an set_tty system call. Set input/output tty of the current process.
    \param tty The tty to set as the process' input/output tty;
    \return The tty just set. For some reason.
*/
uint32_t set_tty(uint32_t tty) {
	return ksyscall((uint32_t) SC_SET_TTY, tty, 0, 0);
}

//! Make a get_tty system call
/*! Make an get_tty system call. Returns the tty of the current process.
    \return The tty of the current process.
*/
uint32_t get_tty() {
	return ksyscall((uint32_t) SC_GET_TTY, 0, 0, 0);
}
	
