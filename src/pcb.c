#include "pcb.h"
#include "lcd.h"
#include "scheduler.h"


/*! Find and return process by tty
   \param plist Pointer to a pcb_list
   \param ntty TTY to search for
   \return First process with tty ntty or 0 if none found
*/
pcb_t* search_tty(pcb_list *plist, uint32_t ntty) {
	pcb_t *cur = plist->first;
	while(cur != 0) {
		if(cur->tty == ntty) {
			return cur;
		}
		cur = cur->next;
	}
	return 0;
}

/*! Insert process proc at the end of list plist
   \param plist Pointer to a pcb_list
   \param proc Process to insert
   \return void
*/
void insert(pcb_list *plist, pcb_t *proc) {
	if(plist->length == 0) {
		plist->first = proc;
		proc->next = 0;
		plist->length++;
	} else {
		pcb_t *cur = plist->first;
		while(cur->next != 0) {
			cur = cur->next;
		}
		cur->next = proc;
		proc->next = 0;
		plist->length++;
	}
}

/*! Remove process proc from list and return it
   \param plist Pointer to a pcb_list
   \param proc Process to find
   \return Pointer to found process or 0 if not found
*/
pcb_t* remove(pcb_list *plist, pcb_t *proc) {
	if(plist->length != 0) {
		pcb_t *cur = plist->first;
		pcb_t *prev = cur;
		if(cur == proc) {
			plist->first = cur->next;
			plist->length--;
			proc->next = 0;
			return proc;
		} else {
			cur = cur->next;
			while(cur != 0) {
				if(cur == proc) {
					prev->next = proc->next;
					plist->length--;
					proc->next = 0;
					return proc;
				}
				prev = cur;
				cur = cur->next;
			}
		}
	}
	return 0;
}

/*! Remove process with tty ntty from list plist and return it
   \param plist Pointer to a pcb_list
   \param ntty TTY to search for
   \return Pointer to first process with tty ntty or 0 if not found
*/
pcb_t* remove_tty(pcb_list *plist, uint32_t ntty) {
	if(plist->length != 0) {
		pcb_t *cur = plist->first;
		pcb_t *prev = cur;
		if(cur->tty == ntty) {
			plist->first = cur->next;
			plist->length--;
			cur->next = 0;
			return cur;
		} else {
			cur = cur->next;
			while(cur != 0) {
				if(cur->tty == ntty) {
					prev->next = cur->next;
					plist->length--;
					cur->next = 0;
					return cur;
				}
				prev = cur;
				cur = cur->next;
			}
		}
	}
	return 0;
}

//! Removes first process with ID id and wait_child cid or 0 from list plist and returns it.
/*! Removes first process with ID id and wait_child cid or 0 from list plist and returns it.
  \param plist Pointer to pcb_list to search in
  \param id Id of the parent of the process to remove.
  \param cid Id of child parent should be waiting for. Or 0 if any child.
  \return Pointer to pcb_t if search successful otherwise 0;
*/
pcb_t* remove_id_cid(pcb_list *plist, uint32_t id, uint32_t cid) {
	if(plist->length != 0) {
		pcb_t *cur = plist->first;
		pcb_t *prev = cur;
		if(cur->id == id && (cur->wait_child == cid || cur->wait_child == 0)) {
			plist->first = cur->next;
			plist->length--;
			cur->next = 0;
			return cur;
		} else {
			cur = cur->next;
			while(cur != 0) {
				if(cur->id == id && (cur->wait_child == cid || cur->wait_child == 0)) {
					prev->next = cur->next;
					plist->length--;
					cur->next = 0;
					return cur;
				}
				prev = cur;
				cur = cur->next;
			}
		}
	}
	return 0;
}

//! Removes first process with ID id from list plist and returns it.
/*! Removes first process ID id from list plist and returns it.
  \param plist Pointer to pcb_list to search in
  \param id Id of the process to remove.
  \return Pointer to pcb_t if search successful otherwise 0;
*/
pcb_t* remove_id(pcb_list *plist, uint32_t id) {
	if(plist->length != 0) {
		pcb_t *cur = plist->first;
		pcb_t *prev = cur;
		if(cur->id == id) {
			plist->first = cur->next;
			plist->length--;
			cur->next = 0;
			return cur;
		} else {
			cur = cur->next;
			while(cur != 0) {
				if(cur->id == id) {
					prev->next = cur->next;
					plist->length--;
					cur->next = 0;
					return cur;
				}
				prev = cur;
				cur = cur->next;
			}
		}
	}
	return 0;
}

//! Removes first process with PID pid from list plist and returns it.
/*! Removes first process PID pid from list plist and returns it.
  \param plist Pointer to pcb_list to search in
  \param pid ID of the parent of the process to remove.
  \return Pointer to pcb_t if search successful otherwise 0;
*/
pcb_t* remove_pid(pcb_list *plist, uint32_t pid) {
	if(plist->length != 0) {
		pcb_t *cur = plist->first;
		pcb_t *prev = cur;
		if(cur->pid == pid) {
			plist->first = cur->next;
			plist->length--;
			cur->next = 0;
			return cur;
		} else {
			cur = cur->next;
			while(cur != 0) {
				if(cur->pid == pid) {
					prev->next = cur->next;
					plist->length--;
					cur->next = 0;
					return cur;
				}
				prev = cur;
				cur = cur->next;
			}
		}
	}
	return 0;
}


//! Removes and returns the first pcb in plist
/*! Removes and return the first pcb in plist
   \param plist - A pcb_list
   \return  Pointer to first pcb in plist or 0 if plist is empty
*/
pcb_t* pop(pcb_list *plist) {
	if(plist->length == 0)
		return 0;

	pcb_t *proc = plist->first;
	plist->first = proc->next;
	plist->length--;
	proc->next = 0;
	return proc;
}

//! Inserts proc first in plist
/*! Inserts proc first in plist
   \param plist Pointer to a pcb_list
   \param proc  Pointer to a pcb_t
   \return void
*/
void push(pcb_list *plist, pcb_t *proc) {
	proc->next = plist->first;
	plist->first = proc;
	plist->length++;
}

//! Sorted insert based on compare function.
/*! Inserts process proc into list plist based on compare function compare.
   |param plist Pointer to a pcb_list.
 * |param proc Pointer to a pcb_t.
 * |param cmp_func Pointer to a compare function ( uint32_t func(pcb_t *a, pcb_t *b) ).
 * |return void.
 */
void s_insert(pcb_list *plist, pcb_t *proc, cmp_func compare) {
	if(plist->length == 0) {
		plist->first = proc;
		plist->length++;
	} else {
		if((*compare)(proc, plist->first) == -1 || (*compare)(proc, plist->first) == 0) {
			proc->next = plist->first;
			plist->first = proc;
			plist->length++;
			return;
		}
		pcb_t *cur = plist->first;
		while(cur != 0) {
			if(cur->next == 0) {
				proc->next = 0;
				cur->next = proc;
				plist->length++;
				return;
			} else if((*compare)(proc, cur->next) == -1 || (*compare)(proc, cur->next) == 0) {
				proc->next = cur->next;
				cur->next = proc;
				plist->length++;
				return;
			}
			cur = cur->next;
		}
		cur->next = proc;
		plist->length++;
	}
}



