#include "kmalloc.h"
#include "scheduler.h"

#define POOL_SIZE 500000
#define NALLOC 512 /* minimum #units to request */

static Header base;					/* empty list to get started */
static Header *freep = 0;			/* start of free list */
static Header mem_pool[POOL_SIZE]; 	// 8MB, each header is 16 bytes. In BSS
static int next_i = 0;




/* morecore - Add more memory to free list
 * min number of units allowed is defined in NALLOC
 * Args:
 * uint32_t nu - number of units to add to free list
 *
 * Returns:
 * pointer to free list if successful otherwise NULL
 */
Header *morecore(unsigned nu)
{
	Header *up;
	if (nu < NALLOC) {
		nu = NALLOC;
	}
	if((next_i + nu) > (POOL_SIZE - 1)) {
		return 0;
	}

	up = &mem_pool[next_i];
	next_i += nu;
	up->s.size = nu;
	kfree((void *)(up+1));
	return freep;
}


/* k&r malloc: general-purpose storage allocator
 * Rounds up to nearest multiple of sizeof(Header) and plus 1*sizeof(Header)
 * to make room for Header.
 */

/* malloc - allocate memory
 * Args:
 * uint32_t nbytes - number of bytes to allocate
 *
 * Returns:
 * pointer to allocated memory if successful otherwise NULL (out of memory)
 */

void kmalloc(unsigned nbytes)
{
	Header *p, *prevp;
	unsigned nunits;
	
	nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1; //Number of "units" to allocate

	prevp = freep;
	if (freep == 0) { /* no free list yet */
		base.s.ptr = freep = prevp = &base;
		base.s.size = 0;
	}

	// Search free list for big enough chunk
	for (p = prevp->s.ptr; ; prevp = p, p = p->s.ptr) {
		if (p->s.size >= nunits) { 			/* big enough */
			if (p->s.size == nunits) {		/* exactly */
				prevp->s.ptr = p->s.ptr;
			} else {						/* allocate tail end */
				p->s.size -= nunits;		
				p += p->s.size;
				p->s.size = nunits;
			}
			freep = prevp;
			p += 1;
			set_return((uint32_t)p);
			p -= 1;
			break;
		}
		if (p == freep) /* wrapped around free list */
			if ((p = morecore(nunits)) == 0) {
				set_return((uint32_t)0); /* none left */
				break;
			}
	}
}

void* exc_malloc(unsigned nbytes)
{
	Header *p, *prevp;
	unsigned nunits;
	
	nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1; //Number of "units" to allocate

	prevp = freep;
	if (freep == 0) { /* no free list yet */
		base.s.ptr = freep = prevp = &base;
		base.s.size = 0;
	}

	// Search free list for big enough chunk
	for (p = prevp->s.ptr; ; prevp = p, p = p->s.ptr) {
		if (p->s.size >= nunits) { 			/* big enough */
			if (p->s.size == nunits) {		/* exactly */
				prevp->s.ptr = p->s.ptr;
			} else {						/* allocate tail end */
				p->s.size -= nunits;		
				p += p->s.size;
				p->s.size = nunits;
			}
			freep = prevp;
			return (void*)(p+1);
		}
		if (p == freep) /* wrapped around free list */
			if ((p = morecore(nunits)) == 0) {
				return (void*)0; /* none left */
			}
	}
}


/* free - Returns allocated memory to the free list
 * Args:
 * void *ap - pointer to the start of the allocated memory
 *
 * Returns:
 * nothing
 */
void kfree(void *ap)
{
	Header *bp, *p;
	bp = (Header *)ap - 1; /* point to block header */

	/* Loop until bp is between two blocks OR (if in loop) until end of list
	 * is reached and bp is after the last block or before the first block
	 */
	
	for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr) {
		//Check if end of list
		if (p >= p->s.ptr && (bp > p || bp < p->s.ptr)) {
			break; /* freed block at start or end of arena */
		}
	}

	// p < bp > p->s.ptr OR bp is at either end of list

	/* If bp is adjacent to p->s.ptr (bp < p->s.ptr)
	 * join them with bp as new header
	 * else bp should point to the next block
	 */
	if (bp + bp->s.size == p->s.ptr) { 		/* join to upper neighbour */
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
	} else {
		bp->s.ptr = p->s.ptr;
	}
	/* If bp is adjacent to p (p < bp)
	 * join them with p as header
	 * else p should point to bp
	 */
	if (p + p->s.size == bp) {				/* join to lower neighbour */
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
	} else {
		p->s.ptr = bp;
	}

	//Set freep to current pos
	freep = p;
}
