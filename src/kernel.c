/* kernel.c
 */
#include "asm.h" 
#include "malta.h" 
#include "debug.h" 
#include "uart.h" 
#include "lcd.h" 
#include "scheduler.h" 
#include "syscall.h" 
#include "shell.h" 
#include "clock.h" 
 
 
void shell_creator(uint32_t a) { 
	int i = 0; 
	i = fork(); 
	if(i == 0) { 
		exec(&shell_main, "shell tty0"); 
	} 
 
	int b = 0; 
	b = fork(); 
	if(b == 0) { 
		set_tty(DEV_TTY1); 
		exec(&shell_main, "shell tty1"); 
	} 
	uint32_t hej; 
	wait(&hej); 
	wait(&hej); 
} 
 
 
//! Sets up the system, loads shell and enters idle loop. 
 
void kinit() { 
    /* Initialize UART */ 
	init_uart(); 
	//printstr("UART initialized\n"); 
	/* Put '0' on the Malta display. */ 
	display_word(0); 
 
	init_scheduler(); 
	//printstr("Scheduler initialized\n"); 

	 
	create_process(&clock_init, "clock", 42); 
	create_process(&shell_creator, "shell_loader", 0); 

	/* Update the status register to enable timer interrupts. 
	 * First arg: Clear IE, EXL, ERL, UM, IM7-IM0, BEV 
	 * 2nd arg: Enable IE, CU0, IM3, IM7 
	 * Timer seems to be IM7, UART seems to be IM0 
	 * Can't find in documentation, based on example code 
	 */  
	kset_sr(0xFFBF00E8, 0x10008401); 
	//printline("Interrupts enabled\n"); 
 
	 
	/* Forever do nothing. 
	 * This is our idle loop 
	 */ 
 
	while(1); 
	printf("Ooooooooh we shouldn't be here\n"); 
} 

 
/* kexception: 
 *   Application-specific exception handler, called after registers 
 *   have been saved. 
 */ 
void kexception() 
{ 
  //static int i = 0; 
	uint32_t timer; 
	uint32_t new_timer; 
	cause_reg_t cause; 
	registers_t* caller_regs; 
	//uint32_t sc_return = 0; 
 
	/* Make sure that we are here because of a timer interrupt. */ 
	cause.reg = kget_cause(); 
	if(cause.field.exc == 0) {    /* External interrupt */ 
		/* TIMER INTERRUPT */ 
		if(cause.field.ip & 0x80) { 
			timer = kget_count() / timer_msec; 
			new_timer = handle_timer(timer); 
			handle_scheduler(); 
			/* Reload timer for another 100 ms (simulated time) */ 
			kload_timer(new_timer * timer_msec); 
		} 
 
		/* UART INTERRUPT */ 
		if (cause.field.ip & 4) { 
			handle_uart(); 
			/* Acknowledge UART interrupt. */ 
			kset_cause(~0x1000, 0); 
		} 
 
   
		/* Icrease the number on the Malta display. */ 
		//	display_word(++i); 
	} else if(cause.field.exc == 8) { 
		/* Load registers to get arguments */ 
		caller_regs = kget_registers(); 
 
		/* Advance PC to next instruction */ 
		caller_regs->epc_reg += 4; 
 
		/* Handle syscall */ 
		handle_syscall(caller_regs->a_reg[0], 
								   caller_regs->a_reg[1], 
								   caller_regs->a_reg[2], 
								   caller_regs->a_reg[3]); 
 
		/* Set return value for caller */ 
		//caller_regs->v_reg[0] = sc_return; 
		 
 
		/* Acknowledge syscall exception */ 
		kset_cause(~0x60, 0); 
	} else { 
//		debug_printf("Cause: %x\n", cause.reg); 
		//printstr("Wrong interrupt\n"); 
		handle_uart(); 
	} 
} 
