/*! \file scheduler.c
    \brief The scheduler and assorted functions for working with processes
    
	This file contains process lists, functions for suspending, waking, forking,
	sleeping and other actions on processes.
*/
#include "scheduler.h"
#include "timer.h"
#include "shell.h"
#include "pcb.h"


/*! \def NUM_PROCS
    The number of processes to allocate space for
*/
#define NUM_PROCS 25

static pcb_t proc_list[NUM_PROCS];
static pcb_list ready_list;
static pcb_list wait_io_list;
static pcb_list wait_time_list;
static pcb_list free_procs;
static pcb_list wait_list;
static pcb_list exit_list;
static pcb_list wait_sem_list;
static pcb_list wait_mail;


static uint32_t current_proc = 0;


//! Copy string from b to a
/*! Copies the string b to a
    \param a Pointer to destination memory.
    \param b Pointer to null terminated string.
    \return void
*/
void strcpy(char *a, char *b) {
	while(*a != '\0') {
		*b++ = *a++;
		
	}
	*b = '\0';

}



/*! Return pointer to PCB with id pid
    \param pid Id of process
    \return Pointer to process with ID pid
*/
pcb_t *find_pcb(uint32_t pid) {
	return &(proc_list[pid]);
}

//! Initializes the scheduler.
/*! Initializes the scheduler by setting process IDs, statuses, ttys and also sets up the init process as the
    first process in the allocated space. It then sets the timer.
*/
void init_scheduler() {
	// Set up PCBs, set id and add to free list
	for(int i = 1; i < NUM_PROCS; i++) {
		proc_list[i].id = i;
		proc_list[i].pid = 0;
		proc_list[i].status = STAT_DEAD;
		proc_list[i].next = 0;
		proc_list[i].tty = DEV_TTY0;
		insert(&free_procs, &proc_list[i]);
	}

	// Set up init process, id 0, set as running
	kset_registers(&proc_list[0].registers);
	proc_list[0].status = STAT_READY;
	proc_list[0].id = 0;
	proc_list[0].pid = 0;
	proc_list[0].tty = DEV_TTY0;
	current_proc = 0;
	strcpy("init", &proc_list[0].name[0]);
	

	/* Initialise timer to interrupt in 100 ms (simulated time). */
	kload_timer(100 * timer_msec);
}

//! Handles completed line reads by uart.
/*! Is run when an line break is read by the uart and
    an input buffer is set by set_input(). It's called by handle_uart in uart.c
    Input is always directed to the first process with ntty as tty in waiting_io_list
    Number of characters read is set in v0 register of the process.
 
    \param n_chars Number of characters put in buffer
    \param ntty The tty the line was read from
    \return void. 
*/

void sched_line_read(uint32_t n_chars, uint32_t ntty) {
	/* Complete line read by UART
	 * Wake up first proc waiting for input
	 */
	if(wait_io_list.length != 0) {
		pcb_t *proc = remove_tty(&wait_io_list, ntty);
		proc->registers.v_reg[0] = n_chars;
		proc->status = STAT_READY;
		insert(&ready_list, proc);
	}
	if(wait_io_list.length != 0) {
		pcb_t *proc = search_tty(&wait_io_list, ntty);
		if(proc != 0) {
			set_input((char*)(proc->buf), proc->tty);
		}
	}
}

/*! Add process to input waiting list and try to set
    as current input.
 
    \param buf A pointer to memory where input will be written
*/

void sched_wait_input(char *buf) {
	//current_proc is the one that made the system call
	pcb_t *proc = &proc_list[current_proc];
	proc->buf = buf;
	proc->status = STAT_WAIT_IO;
	set_input(buf, proc->tty); //won't set if busy
	insert(&wait_io_list, proc);
	handle_scheduler();
	
}	

/*! compare function for s_insert helper for sched_sleep
    sorts by time to wait.
*/
int32_t sort_time(pcb_t *a, pcb_t *b) {
	if(a->time < b->time)
		return -1;
	if(a->time > b->time)
		return 1;

	return 0;
}

/*! Put a process to sleep for msecs milliseconds
    \param msecs Time to sleep in milliseconds
 */

void sched_sleep(uint32_t msecs) {
	pcb_t *proc = &proc_list[current_proc];
	proc->time = msecs;
//	remove(&ready_list, proc);
	proc->status = STAT_WAIT_TIME;
	s_insert(&wait_time_list, proc, &sort_time);
	handle_scheduler();
}

/*! Handles timer events. Updates time left for all
    sleeping processes and wakes those whose time is up.
    Returns shortest remaining timer if it's less than 100ms.
    
    \param time Time in milliseconds since last timer event
    \return Length of next quantum in millisecond.
 */
uint32_t handle_timer(uint32_t time) {
	if(wait_time_list.length == 0) {
		return (100);
	} else {
		pcb_t *cur = wait_time_list.first;
		while(cur != 0 && cur->time <= time) {
			cur->time = 0;
			cur = pop(&wait_time_list);
			cur->status = STAT_READY;
			insert(&ready_list, cur);
			cur = wait_time_list.first;
		}
		while(cur != 0) {
			cur->time -= time;
			cur = cur->next;
		}
	}
	if(wait_time_list.length != 0) {
		if(wait_time_list.first->time < 100)
			return (wait_time_list.first->time);
	}

	return (100);
}	
			

/*! Schedules processes in a simple FIFO manner. If the last running process has not gone
    into waiting it is added to the back of the ready list and then the first process in
    the ready list is selected for running. If the ready list is empty the init process
    will be selected for running. Context switch is handled by kset_registers which sets
    where registers will be set from upon returning from an exception.

    current_proc holds the id of the current running process.
    Status values are defined in scheduler.h
 */

void handle_scheduler() {
	if(current_proc != 0 && (proc_list[current_proc].status == STAT_RUNNING || proc_list[current_proc].status == STAT_READY)) {
		proc_list[current_proc].status = STAT_READY;
		insert(&ready_list, &proc_list[current_proc]);
	}
	
	if(ready_list.length != 0) {
		proc_list[0].status = STAT_READY;
		pcb_t *proc = pop(&ready_list);
		proc->status = STAT_RUNNING;
		kset_registers(&proc->registers);
		current_proc = proc->id;
//		uint32_t mail = 1;
//		while(mail != 0) mail = check_mail(proc);
	} else {
		kset_registers(&proc_list[0].registers);
		proc_list[0].status = STAT_RUNNING;
		current_proc = 0;
	}
}

/*! Creates a new process that starts running function func.
    Must be run in an exception. Is not used by a system call.
	

    \param func Address of the function to run.
    \param name Name of the new process.
    \param arg1 Argument to the function func.
    \return Id of the new process.
*/
uint32_t create_process(void *func, char *name, uint32_t arg1) {
	if(free_procs.length == 0 || func == 0) {
		//Out of memory
		return -1;
	}
	pcb_t *new_proc = pop(&free_procs);
	new_proc->status = STAT_READY;
	strcpy(name, &new_proc->name[0]);
	new_proc->pid = current_proc;
	new_proc->registers.sp_reg = (uint32_t)&new_proc->stack.endstack;
	new_proc->registers.epc_reg = (uint32_t)func;
	new_proc->registers.gp_reg = (uint32_t)0x80000000;
	new_proc->registers.a_reg[0] = arg1;
	new_proc->tty = DEV_TTY0;
	insert(&ready_list, new_proc);
	return new_proc->id;
}
	
/*! Prints a list of all processes to the tty of the current process */
void sched_printprocs() {
	char *stat = "--------------------";
	debug_printf("PID\t\tNAME\t\tPARENT\t\tSTATE\n");
	for(int i = 0; i < NUM_PROCS; i++) {
		if(proc_list[i].status != STAT_DEAD) {
			switch(proc_list[i].status) {
			case STAT_WAITING:
				stat = "WAITING";
				break;
			case STAT_READY:
				stat = "READY";
				break;
			case STAT_RUNNING:
				stat = "RUNNING";
				break;
			case STAT_WAIT_IO:
				stat = "WAIT_IO";
				break;
			case STAT_WAIT_TIME:
				stat = "WAIT_TIME";
				break;
			case STAT_WAIT_SEM:
				stat = "WAIT_SEM";
				break;
			case STAT_WAIT_PARENT:
				stat = "WAIT_PARENT";
				break;
			case STAT_WAIT_MAIL:
				stat = "WAIT_MAIL";
				break;
			default:
				stat = "DEAD";
			}

			debug_printf("%d\t\t%s\t\t%d\t\t%s\n", proc_list[i].id, proc_list[i].name, proc_list[i].pid, stat);
		}
	}
}


/* Makes a copy of the current process by copying the entire PCB sans
   id, status, pid, buf, exit_status, wait_child.
   Pid of the new process is set to the id of the current process.
   All values in parent registers or on parent stack that is a valid address to the
   parent stack is converted to point to the new stack.

  \return In parent: id of child or -1 for error. In child: 0.
*/
void sched_fork() {
	if(free_procs.length == 0) {
		proc_list[current_proc].registers.v_reg[0] = (uint32_t)-1;
		return;
	}
	pcb_t *new_proc = pop(&free_procs);
	pcb_t *cur_proc = &proc_list[current_proc];

	// Set parent id
	new_proc->pid = cur_proc->id;

	// Set as ready
	new_proc->status = STAT_READY;
	new_proc->exit_status = 0;
	new_proc->wait_child = 0;
	new_proc->tty = cur_proc->tty;

	// Not in a list yet
	new_proc->next = 0;

	new_proc->buf = 0; // can't be in a readline-call
	new_proc->time = cur_proc->time;
	strcpy(cur_proc->name, new_proc->name);

	//Get stack limits and offset between stacks
	uint32_t cs_high = (uint32_t)&cur_proc->stack.endstack;
	uint32_t cs_low = (uint32_t)&cur_proc->stack.stackspace[0];
	uint32_t ns_high = (uint32_t)&new_proc->stack.endstack;

	//Offset between the processes
	int32_t s_offset = 0;
	s_offset = ns_high - cs_high;

	//Copy registers, convert addresses pointing to stack
	uint32_t *src = &cur_proc->registers.at_reg;
	uint32_t *dst = &new_proc->registers.at_reg;
	while(src <= &cur_proc->registers.gp_reg) {
		if(cs_high > *src && cs_low < *src) {
			*dst = (uint32_t)(*src + s_offset);
		} else {
			*dst = *src;
		}
		src++;
		dst++;
	}

	//Copy stack, convert addresses pointing to stack
	src = (uint32_t*)cur_proc->registers.sp_reg;
	dst = (uint32_t*)new_proc->registers.sp_reg;
	while(src <= (uint32_t*)&cur_proc->stack.endstack) {
		if(cs_high > *src && cs_low < *src) {
			if(cs_high > ns_high) {
				*dst = (*src) - s_offset;
			} else {
				*dst = (*src) + s_offset;
			}
		} else {
			*dst = *src;
		}
		dst++;
		src++;
	}

	//Set return value
	new_proc->registers.v_reg[0] = 0;
	// Add child to ready queue
	insert(&ready_list, new_proc);

	// Return childs pid to parent
	set_return(new_proc->id);	
}


/*! Resets all data except id and pid then loads a new function into the process.
  \param func The address of a function.
  \param name Pointer to string with name of new process.
  \return void.
*/
void sched_exec(void* func, char *name) {
	pcb_t *cur_proc = &proc_list[current_proc];
	cur_proc->status = STAT_READY;
	cur_proc->next = 0;
	cur_proc->buf = 0;
	cur_proc->time = 0;
	strcpy(name, cur_proc->name);
	cur_proc->registers.at_reg = 0;
    cur_proc->registers.v_reg[0] = 0;
	cur_proc->registers.v_reg[1] = 0;
    cur_proc->registers.a_reg[0] = 0;
	cur_proc->registers.a_reg[1] = 0;
	cur_proc->registers.a_reg[2] = 0;
	cur_proc->registers.a_reg[3] = 0;
	for(int i = 0; i < 10; i++) {
		cur_proc->registers.t_reg[i] = 0;
	}
	for(int i = 0; i < 8; i++) {
		cur_proc->registers.s_reg[i] = 0;
	}
    cur_proc->registers.sp_reg = (uint32_t)&cur_proc->stack.endstack;
	cur_proc->registers.sp_reg -= 32;
    cur_proc->registers.fp_reg = 0;
    cur_proc->registers.ra_reg = 0;
    cur_proc->registers.epc_reg = (uint32_t)func;
    cur_proc->registers.gp_reg = 0x80000000;

	for(int i = 0; i < STACK_SIZE-1; i++) {
		cur_proc->stack.stackspace[i] = 0;
	}
	cur_proc->stack.endstack = 0;
}

/*! Resets all data except id and pid then loads a new function into the process.
  \param func The address of a function.
  \param name Pointer to string with name of new process.
  \param arg1 The argument to the function func
  \return void.
*/
void sched_exec1(void* func, char *name, uint32_t arg1) {
	pcb_t *cur_proc = &proc_list[current_proc];
	cur_proc->status = STAT_READY;
	cur_proc->next = 0;
	cur_proc->buf = 0;
	cur_proc->time = 0;
	strcpy(name, cur_proc->name);
	cur_proc->registers.at_reg = 0;
    cur_proc->registers.v_reg[0] = 0;
	cur_proc->registers.v_reg[1] = 0;
    cur_proc->registers.a_reg[0] = arg1;
	cur_proc->registers.a_reg[1] = 0;
	cur_proc->registers.a_reg[2] = 0;
	cur_proc->registers.a_reg[3] = 0;
	for(int i = 0; i < 10; i++) {
		cur_proc->registers.t_reg[i] = 0;
	}
	for(int i = 0; i < 8; i++) {
		cur_proc->registers.s_reg[i] = 0;
	}
    cur_proc->registers.sp_reg = (uint32_t)&cur_proc->stack.endstack;
	cur_proc->registers.sp_reg -= 32;
    cur_proc->registers.fp_reg = 0;
    cur_proc->registers.ra_reg = 0;
    cur_proc->registers.epc_reg = (uint32_t)func;
    cur_proc->registers.gp_reg = 0x80000000;

	for(int i = 0; i < STACK_SIZE-1; i++) {
		cur_proc->stack.stackspace[i] = 0;
	}
	cur_proc->stack.endstack = 0;
}



//! Ends the current process.
/*! Ends the current process. Zeroes out data. Returns stat to parent or waits for
    parent to run wait();
  \param stat Exit status of process.
  \return void.
*/
void sched_exit(uint32_t stat) {
	pcb_t *cur_proc = &proc_list[current_proc];
	cur_proc->status = STAT_DEAD;
	cur_proc->next = 0;
	cur_proc->buf = 0;
	cur_proc->time = 0;
	cur_proc->tty = DEV_TTY0;
	cur_proc->registers.at_reg = 0;
    cur_proc->registers.v_reg[0] = 0;
	cur_proc->registers.v_reg[1] = 0;
    cur_proc->registers.a_reg[0] = 0;
	cur_proc->registers.a_reg[1] = 0;
	cur_proc->registers.a_reg[2] = 0;
	cur_proc->registers.a_reg[3] = 0;
	for(int i = 0; i < 10; i++) {
		cur_proc->registers.t_reg[i] = 0;
	}
	for(int i = 0; i < 8; i++) {
		cur_proc->registers.s_reg[i] = 0;
	}
    cur_proc->registers.sp_reg = 0;
    cur_proc->registers.fp_reg = 0;
    cur_proc->registers.ra_reg = 0;
    cur_proc->registers.epc_reg = 0;
    cur_proc->registers.gp_reg = 0;

	for(int i = 0; i < STACK_SIZE-1; i++) {
		cur_proc->stack.stackspace[i] = 0;
	}
	cur_proc->stack.endstack = 0;
	cur_proc->time = 0;

	// Check if parent waits
	pcb_t *parent = remove_id_cid(&wait_list, cur_proc->pid, cur_proc->id);

	// If parent waits, wake it. Collect pointer to status from stack;
	if(parent != 0) {
		uint32_t *save_stat = (uint32_t*)parent->exit_status;
		*save_stat = stat;
		parent->status = STAT_READY;
		parent->wait_child = 0;
		insert(&ready_list, parent);
		cur_proc->pid = 0;
		insert(&free_procs, cur_proc);
		parent->registers.v_reg[0] = cur_proc->id;
	} else {
		// No parent waits, we wait for it
		// Hold exit status in time field
		cur_proc->exit_status = stat;
		cur_proc->status = STAT_WAIT_PARENT;
		insert(&exit_list, cur_proc);
	}
	// Now need to schedule in another process
	handle_scheduler();
}
//! Wait for child process to exit.
/*! Make current process wait for a child process (any child process) to exit.
  \param status Pointer to uint32_t where exit status of child is stored.
  \return Id of child.
*/
void sched_wait(uint32_t *status) {
	pcb_t *cur_proc = &proc_list[current_proc];
	pcb_t *child = remove_pid(&exit_list, cur_proc->id);
	if(child != 0) {
		cur_proc->registers.v_reg[0] = child->id;
		//Set return status
		*status = child->exit_status;
		child->exit_status = 0;
		child->pid = 0;
		child->status = STAT_DEAD;
		insert(&free_procs, child);
	} else {
		cur_proc->exit_status = (uint32_t)status;
		cur_proc->status = STAT_WAITING;
		cur_proc->wait_child = 0;
		insert(&wait_list, cur_proc);
		handle_scheduler();
	}
}

//! Wait for specific child process to exit.
/*! Make current process wait for a specific child process to exit.
  \param status Pointer to uint32_t where exit status of child is stored.
  \param id Id of child to wait for.
  \return Id of child.
*/

void sched_waitid(uint32_t *status, uint32_t id) {
	pcb_t *cur_proc = &proc_list[current_proc];
	pcb_t *child = remove_id(&exit_list, id);
	if(child != 0) {
		cur_proc->registers.v_reg[0] = child->id;
		//Set return status
		*status = child->exit_status;
		child->exit_status = 0;
		child->pid = 0;
		child->status = STAT_DEAD;
		insert(&free_procs, child);
	} else {
		cur_proc->exit_status = (uint32_t)status;
		cur_proc->status = STAT_WAITING;
		cur_proc->wait_child = id;
		insert(&wait_list, cur_proc);
		handle_scheduler();
	}
}
/*! Returns the id of the current process by setting it in v0 register of
    the current process.
*/
void sched_getid() {
	set_return(current_proc);
}

/*! Return the id of the parent of the current process by setting it in v0
    register of the current process.
*/
void sched_getpid() {
	set_return(proc_list[current_proc].pid);
}

/*! Puts the process to sleep while waiting for the semaphore s to be signalled.
    \param s The semaphore to wait for a signal on.
*/
void sem_sleep(sem_t *s)
{
    if (*s) {
        (*s)--;
        return;
    }

    pcb_t *this = &proc_list[current_proc];
    this->status = STAT_WAIT_SEM;
    this->sem = s;
    insert(&wait_sem_list, this);
    handle_scheduler();
}


/*! Wakes the first process that is waiting for semaphore *s
    \param s The semaphore the process should be waiting on in order to be woken.
*/
void sem_wakeup(sem_t *s)
{
    if (*s) {
        (*s)++;
        return;
    }

    pcb_t **cur, *p;
    cur = &wait_sem_list.first;

    while (*cur) {
        if ((*cur)->sem == s) {
            p = *cur;
            *cur = (*cur)->next;
            wait_sem_list.length--;

            p->status = STAT_READY;
            insert(&ready_list, p);
            return;
        }
        cur = &(*cur)->next;
    }
    (*s)++;
}

/*! Set tty of current process. DEV_TTY0 or DEV_TTY1.
    \param ntty The new tty of the process.
*/
void sched_set_tty(uint32_t ntty) {
	proc_list[current_proc].tty = ntty;
	set_return(proc_list[current_proc].tty);
}

/*! Get the tty of the current process. Return it by setting v0 register
    of the current process.
*/

void sched_get_tty() {
	set_return(proc_list[current_proc].tty);
}

/*! Set return value in v0 register of the current process.
    It will be picked up as the return value of ksyscall.
    \param ret The return value to set for the current process.
*/
void set_return(uint32_t ret) {
	proc_list[current_proc].registers.v_reg[0] = ret;
}

/*! Return tty of current process. To be used while already in an exception. */
uint32_t k_get_tty() {
	return proc_list[current_proc].tty;
}

/******** IPC FUNCTIONS *********/

uint32_t local_receive(mail_t msg_space) {
	pcb_t *p = &proc_list[current_proc];
	if(p->mail == 0) {
		return 0;
	} else {
		msg_space->ctrl = p->mail->ctrl;
		msg_space->data = p->mail->data;
		msg_space->next = 0;
		mail_t tmp = p->mail;
		p->mail = p->mail->next;
		rm_mail(tmp);
		return 1;
	}
}

void ipc_receive(mail_t msg_space) {
	if(!local_receive(msg_space)) {
		set_return(0);
	} else {
		set_return(1);
	}
}



void ipc_receive_wait(mail_t msg_space) {
	if(!local_receive(msg_space)) {
		pcb_t *proc = &proc_list[current_proc];
		proc->status = STAT_WAIT_MAIL;
		proc->wait_mail = msg_space;
		insert(&wait_mail, proc);
		handle_scheduler();
	}
}

//Takes data and ctrl number, creates a message element which is sent to process pid
//Returns pid if successful, -1 if not
void ipc_send(uint32_t data, uint32_t ctrl, uint32_t pid) {
	if(pid >= 0 && pid < NUM_PROCS) {
		pcb_t *p = 0;
		p = remove_id(&wait_mail, pid);
		if(p) {
			p->status = STAT_READY;
			insert(&ready_list, p);
			p->wait_mail->data = data;
			p->wait_mail->ctrl = ctrl;
			p->wait_mail->next = 0;
			set_return(p->id);
		} else {
			p = &proc_list[current_proc];
			if(p) {
				mail_t msg = mk_mail(data, ctrl);
				push_mail(msg, p);
				set_return(p->id);
			} else {
				set_return(-1);
			}
		}
	} else {
		set_return(-1);
	}
}

