/* example - function that prints a string to the terminal window  */
#include "malta.h"
#include "asm.h"
#include "uart.h"
#include "debug.h"
#include "scheduler.h"
#include "syscall.h"

/* DATA */
static bounded_fifo_t bfifo; 	//Output buffer
static char *input_buf = 0;		//Input buffer to be set by other processes
static uint32_t input_i = 0;	//Index for the input buffer

static bounded_fifo_t bfifo2;
static char *input_buf2 = 0;
static uint32_t input_i2 = 0;

/* init_uart - Initialize everything needed by the uart
 */
void init_uart() {
	/* Set UART word length ('3' meaning 8 bits).
	 * Do this early to enable debug printouts (e.g. kdebug_print).
	 */
	tty->lcr.field.wls = 3;

	bfifo.length = 0;
	bfifo.put = 0;
	bfifo.get = 0;
  
	// SR must also be set right, in kernel.c code

	/* Generate interrupts when data is received by UART. */
	tty->ier.field.erbfi = 1;

	/* Some obscure bit that need to be set for UART interrupts to work. */
	tty->mcr.field.out2 = 1;

	//Second UART
	tty2->lcr.field.wls = 3;
	bfifo2.length = 0;
	bfifo2.put = 0;
	bfifo2.get = 0;

	tty2->ier.field.erbfi = 1;
	tty2->mcr.field.out2 = 1;
}


/* bfifo_put: Inserts a character at the end of the queue. */
static void bfifo_put(bounded_fifo_t* bfifo, uint8_t ch)
{
	/* Make sure the 'bfifo' pointer is not 0. */
	kdebug_assert(bfifo != 0);

	
	bfifo->buf[(bfifo->put++)%FIFO_SIZE] = ch;
	bfifo->length++;
	
}

/* bfifo_get: Returns a character removed from the front of the queue. */
static uint8_t bfifo_get(bounded_fifo_t* bfifo)
{
	//int i;
	uint8_t ch;

	/* Make sure the 'bfifo' pointer is not 0, and that queue is not empty. */
	kdebug_assert(bfifo != 0);
	kdebug_assert(bfifo->length > 0);

	bfifo->length--;
	ch = bfifo->buf[(bfifo->get++)%FIFO_SIZE];
	return ch;
}


void putc(char c, uint32_t ntty) 
{
	if(ntty == 0) {
		bfifo_put(&bfifo,c);
		tty->ier.field.etbei = (bfifo.length > 0);
	} else if (ntty == 1) {
		bfifo_put(&bfifo2,c);
		tty2->ier.field.etbei = (bfifo2.length > 0);
	}
}

/* printstr - Prints the string
 * Should be used for debugging in exceptions as
 * both printline and printf generates interrupts
 *
 * Args:
 * char* print_array - The null terminated string to be printed
 *
 * Returns:
 * void
 */
void printstr(char print_array[])
{
	uint32_t ntty = k_get_tty();
	int i = 0;
	while(print_array[i] != '\0')
    {
		if(print_array[i] == '\n'){ 
			putc('\n', ntty);
			putc('\r', ntty);
		} else {
			putc(print_array[i], ntty);
		}
		i++;
    }
}

/* set_input - Sets the input buffer to in
 * When input buffer is set all input read is saved into it
 * by handle_uart(), when an '\r' is read the scheduler is
 * notified via sched_line_read()
 *
 * Args:
 * char *in - Pointer to memory where input will be saved
 *
 * Returns:
 * void
 */

int32_t set_input(char *in, uint32_t ntty) {
	if(ntty == DEV_TTY0) {
		if(input_buf == 0) {
			input_buf = in;
			input_i = 0;
			return 1;
		} else {
			return 0;
		}
	} else if (ntty == DEV_TTY1) {
		if(input_buf2 == 0) {
			input_buf2 = in;
			input_i2 = 0;
			return 1;
		} else {
			return 0;
		}
	}
	return 1;
}

/* handle_scheduler - Handles UART interrupts
 *
 * We end up here on both ready to transmit and data ready interrupts.
 * When reading data from input it checks if an input buffer is set via
 * set_input and if that is the case it saves input there aswell.
 * When a '\r' is read and input buffer is set it signals the scheduler
 * vi sched_line_read() that a line has been read.
 *
 * Args: void
 * Returns: void
 */

void handle_uart() {
	uint8_t ch = 0;
	if (tty->lsr.field.dr) {
	  /* Data ready: add character to buffer */
	  ch = tty->rbr;
	  if(ch == 8) { //backspace
	    bfifo_put(&bfifo, 8);
	    bfifo_put(&bfifo, ' ');
		  
	  } 
	  bfifo_put(&bfifo, ch);
		if (ch == '\r') {
		  bfifo_put(&bfifo, '\n');
		}
		
		if(input_buf) {
			if(ch == '\r') {
			  /* Should \r be replaced by \0 or \n\0? */
				input_buf[input_i++] = '\0';
				input_buf = 0;
				sched_line_read(input_i, DEV_TTY0);
			} else if(ch == 8) {			//backspace
				if(input_i != 0) {
					input_i--;
				}
			} else {
				input_buf[input_i++] = ch;
			}
		}
			
	}
	if (bfifo.length > 0 && tty->lsr.field.thre) {
		/* Transmitter idle: transmit buffered character */
		tty->thr = bfifo_get(&bfifo);
    
		/* Determine if we should be notified when transmitter becomes idle */
		tty->ier.field.etbei = (bfifo.length > 0);
	}
	
	uint8_t ch2 = 0;
	if (tty2->lsr.field.dr) {
	  /* Data ready: add character to buffer */
	  ch2 = tty2->rbr;
	  if(ch2 == 8) { //backspace
	    bfifo_put(&bfifo2, 8);
	    bfifo_put(&bfifo2, ' ');
		  
	  } 
	  bfifo_put(&bfifo2, ch2);
		if (ch2 == '\r') {
		  bfifo_put(&bfifo2, '\n');
		}
		
		if(input_buf2) {
			if(ch2 == '\r') {
			  /* Should \r be replaced by \0 or \n\0? */
				input_buf2[input_i2++] = '\0';
				input_buf2 = 0;
				sched_line_read(input_i2, DEV_TTY1);
				
			} else if(ch2 == 8) {			//backspace
				if(input_i2 != 0) {
					input_i2--;
				}
			} else {
				input_buf2[input_i2++] = ch2;
			}
		}
			
	}
	if (bfifo2.length > 0 && tty2->lsr.field.thre) {
		/* Transmitter idle: transmit buffered character */
		tty2->thr = bfifo_get(&bfifo2);
    
		/* Determine if we should be notified when transmitter becomes idle */
		tty2->ier.field.etbei = (bfifo2.length > 0);
	}
}
